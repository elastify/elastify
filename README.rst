Installation
============
python3 setup.py develop

Command Line Interface
======================
- elastify
  Indexes a bulk of data.
- querify
  Evaluates queries given in a file (line-wise).
- indices
  Manages your indices in elasticsearch.
- extractor
  Extracts Thesaurus' data about pref/alt-Labels
  to files readable by elasticsearch.
- cutset
  Utily to obtain the intersection of two datasets.


Usage
=====
Some common workflows

Creating a new index
--------------------

